'use strict'

const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
  entry: [
    './src/main.js'
  ],
  output: {
        path:__dirname+ '/dist/',
        filename: "bundle.js",
        publicPath: '/'
    },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      { test: /\.css$/, loader: 'style-loader!css-loader' },
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
  ]
}