import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router);

export default new Router({
	mode: 'hash',
	routes: [
		{
			path: '/',
			component: require('../components/layout.vue').default,
			children: [
				{
					path: '',
					name: 'Index',
					component: require('../pages/blog/index.vue').default
				}
			]
		}
	]
	
});