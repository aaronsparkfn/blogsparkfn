import Vue from 'vue'
import axios from 'axios'

import router from './router'
import App from './App.vue'

import Vuetify from 'vuetify'
 
Vue.use(Vuetify)

// index.js or main.js
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader

axios.defaults.headers.common = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'X-Requested-With': 'XMLHttpRequest'
}

Vue.prototype.$http = axios

axios.defaults.baseURL = `http://sparkfn-blog.herokuapp.com/`;
//axios.defaults.baseURL = 'http://192.168.100.5:8000'
//Vue.config.productionTip = false

new Vue({
	el: '#app',
	render: h => h(App),
	router
});
